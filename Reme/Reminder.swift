//
//  Reminder.swift
//  Reme
//
//  Created by Zach Eriksen on 7/9/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import UIKit

class ReminderColor: UIColor{
    let colors : [UIColor] =
        [.rgba(26, 188, 156,1.0),
         .rgba(46, 204, 113,1.0),
         .rgba(52, 152, 219,1.0),
         .rgba(241, 196, 15,1.0),
         .rgba(230, 126, 34,1.0),
         .rgba(231, 76, 60,1.0),
         .rgba(155, 89, 182,1.0)]
    
}

class ReminderData: NSObject {
    var text : String!
    var date : NSDate!
    var color : UIColor! {
        didSet{
           level = ReminderColor().colors.indexOf(color)
        }
    }
    var level : Int!
    var isDayReminder : Bool! = false
    init(text : String, date : NSDate, level : Int, isDayReminder : Bool) {
        self.text = text
        self.date = date
        self.level = level
        self.isDayReminder = isDayReminder
        self.color = ReminderColor().colors[level]
    }
    
    required init(coder aDecoder: NSCoder) {
        text = aDecoder.decodeObjectForKey("text") as! String
        color = aDecoder.decodeObjectForKey("color") as! UIColor
        date = aDecoder.decodeObjectForKey("date") as! NSDate
        level = aDecoder.decodeIntegerForKey("level")
        
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encodeObject(text, forKey: "text")
        aCoder.encodeObject(color, forKey: "color")
        aCoder.encodeObject(date, forKey: "date")
        aCoder.encodeInteger(level, forKey: "level")
    }
}

class Reminder: UIView {
    var textView : UITextView!
    var bar : UIView!
    var timeButton : UIButton!
    var data : ReminderData!
    
    init(y : CGFloat, data : ReminderData) {
        self.data = data
        super.init(frame: CGRectMake(10, y, screen_width-20, 60))
        backgroundColor = .rgba(238,238,238,1)
        layer.cornerRadius = 7
        clipsToBounds = false
        
        bar = UIView(frame: CGRectMake(0,0,70,frame.size.height))
        bar.backgroundColor = data.color
        bar.layer.cornerRadius = 7
        bar.clipsToBounds = true
        addSubview(bar)
        
        timeButton = UIButton(frame: CGRectMake(5,0,65,frame.size.height))
        formatDate()
        timeButton.titleLabel!.numberOfLines = 2
        timeButton.backgroundColor = data.color
        timeButton.titleLabel!.font = UIFont(name: "Helvetica Neue", size: 15)
        timeButton.titleLabel!.textColor = .whiteColor()
        timeButton.addTarget(self, action: #selector(selectDateColor), forControlEvents: .TouchUpInside)
        addSubview(timeButton)
        
        textView = UITextView(frame: CGRectMake(75,0,frame.size.width-95, frame.size.height))
        textView.backgroundColor = backgroundColor
        textView.textColor = data.color
        textView.text = data.text
        textView.font = UIFont(name: "Helvetica Neue", size: 18)
        textView.textContainer.maximumNumberOfLines = 2
        textView.textContainer.lineBreakMode = .ByWordWrapping
        textView.autocorrectionType = .No
        addSubview(textView)
    }
    
    func formatDate(){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd\nhh:mm"
        let strDate = dateFormatter.stringFromDate(data.date)
        timeButton.setTitle(strDate, forState: .Normal)
    }

    func selectDateColor(){
        selectedReminder = self
        datePicker.date = data.date
        UIView.animateWithDuration(0.5, animations: {
            datePicker.frame = CGRectMake(0, screen_height-200, screen_width, 200)
            colorPicker.frame = CGRectMake(0, screen_height-260, screen_width, 60)
        })
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
