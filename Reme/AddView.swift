//
//  AddView.swift
//  Reme
//
//  Created by Zach Eriksen on 7/20/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import UIKit

class AddView: UIView {
    var reminderDate : NSDate!
    var text : String!
    var color : UIColor!
    var dotArray : [UIView] = []
    var reminder : Reminder!
    var datePicker : UIDatePicker!
    let startY = (screen_height/2)-65
    
    
    init(){
        super.init(frame: CGRectMake(0, 60-screen_height, screen_width, screen_height-60))
        backgroundColor = .whiteColor()
        createDots()
        for dot in dotArray {
            addSubview(dot)
        }
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundTapped)))
        reminder = Reminder(y: startY, data: ReminderData(text: "", date: NSDate(timeIntervalSinceNow: 0 ), level: 3, isDayReminder: true))
        reminder.formatDate()
        let panRecognizer = UIPanGestureRecognizer(target:self, action:#selector(AddView.detectPan(_:)))
        reminder.gestureRecognizers = [panRecognizer]
        addSubview(reminder)
        reminder.timeButton.enabled = false
        datePicker = UIDatePicker(frame: CGRectMake(0,0,screen_width,reminder.frame.origin.y))
        datePicker.addTarget(self, action: #selector(AddView.datePickerValueChanged), forControlEvents: .ValueChanged)
        datePicker.minimumDate = NSDate(timeIntervalSinceNow: 0)
        datePicker.backgroundColor = UIColor.whiteColor()
        addSubview(datePicker)
        
    }
    
    func backgroundTapped(){
        reminder.endEditing(true)
    }
    
    func opening(){
        for view in subviews {
            view.removeFromSuperview()
        }
        backgroundColor = .whiteColor()
        createDots()
        for dot in dotArray {
            addSubview(dot)
        }
        reminder = Reminder(y: startY, data: ReminderData(text: "", date: NSDate(), level: 3, isDayReminder: true))
        let panRecognizer = UIPanGestureRecognizer(target:self, action:#selector(AddView.detectPan(_:)))
        reminder.gestureRecognizers = [panRecognizer]
        reminder.timeButton.enabled = false
        addSubview(reminder)
        datePicker = UIDatePicker(frame: CGRectMake(0,0,screen_width,reminder.frame.origin.y))
        datePicker.addTarget(self, action: #selector(AddView.datePickerValueChanged), forControlEvents: .ValueChanged)
        datePicker.backgroundColor = UIColor.whiteColor()
        addSubview(datePicker)
        reminder.frame = CGRectMake(reminder.frame.origin.x, startY, reminder.frame.size.width, reminder.frame.size.height)
         reminder.textView.becomeFirstResponder()
        UIView.animateWithDuration(0.5, animations: {
                self.frame = CGRectMake(0, 60, screen_width, screen_height-60)
                self.datePicker.frame = CGRectMake(0,0,screen_width,self.reminder.frame.origin.y)
        })
    }
    
    func getReminderData() -> ReminderData{
        reminder.data.text = reminder.textView.text
        return ReminderData(text: reminder.data.text, date: reminder.data.date, level: reminder.data.level, isDayReminder: reminder.data.isDayReminder)
    }
    
    func datePickerValueChanged(sender : UIDatePicker) {
        datePicker.minimumDate = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd\nhh:mma"
        let strDate = dateFormatter.stringFromDate(datePicker.date)
        reminder.timeButton.setTitle(strDate, forState: .Normal)
        reminder.data.date = datePicker.date
    }
    
    func detectPan(sender:UIPanGestureRecognizer){
        reminder.textView.endEditing(true)
        UIView.animateWithDuration(0.5, animations: {
            self.datePicker.frame.origin = CGPointMake(self.datePicker.frame.origin.x,-self.startY)
        })
        checkDotColor()
        reminder.center = CGPointMake(reminder.center.x, sender.locationInView(self).y)
        reminder.data.text = reminder.textView.text
    }
    
    func checkDotColor(){
        for dot in dotArray{
            if reminder.frame.intersects(dot.frame) {
                reminder.data.color = dot.backgroundColor
                reminder.textView.textColor = dot.backgroundColor
                reminder.bar.backgroundColor = dot.backgroundColor
                reminder.timeButton.backgroundColor = dot.backgroundColor
            }
        }
    }
    
    private func createDots(){
        let yInc : CGFloat = (screen_height-80)/CGFloat(ReminderColor().colors.count)
        var currentY : CGFloat = 40
        for color in ReminderColor().colors.reverse() {
            let dot = UIView(frame: CGRectMake(40,currentY,10,10))
            currentY += yInc
            dot.layer.cornerRadius = 5
            dot.clipsToBounds = false
            dot.backgroundColor = color
            dotArray.append(dot)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}