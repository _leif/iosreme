//
//  ViewController.swift
//  Reme
//
//  Created by Zach Eriksen on 7/9/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import UIKit
var sortType : SortType = .time
enum SortType : String {
    case importance = "importance"
    case time = "time"
}

var selectedReminder : Reminder?
var datePicker : UIDatePicker!
var colorPicker : UIView!

class ViewController: UIViewController {
    var scrollView : UIScrollView!
    var topBarView : UIView!
    var filterImageView : UIImageView!
    var cancelImageView : UIImageView!
    var filterButton : UIButton!
    var addButton : UIButton!
    var addButton_cancel : UIButton!
    var addView : AddView!
    var currentReminderY : CGFloat = 70
    var reminders : [Reminder] = []
    var reminderTag = 0
    
    func saveReminders(){
        var remindersData : [ReminderData] = []
        for reminder in reminders {
            remindersData.append(reminder.data)
        }
        let data = NSKeyedArchiver.archivedDataWithRootObject(remindersData)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "reminders")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func loadReminders(){
        print("START:Load")
        guard let data = NSUserDefaults.standardUserDefaults().objectForKey("reminders") as? NSData else {
            print("ERROR:GUARD:data:Load")
            return
        }
        guard let reminderData = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [ReminderData]! else {
            print("ERROR:GUARD:reminderData:Load")
            return
        }
        var loadedReminders : [Reminder] = []
        for data in reminderData {
            loadedReminders.append(Reminder(y: 70, data: data))
        }
        print(loadedReminders)
        reminders = loadedReminders
        sort()
        for reminder in reminders {
            reminder.frame = CGRectMake(reminder.frame.origin.x, currentReminderY, reminder.frame.size.width, reminder.frame.size.height)
            scrollView.addSubview(reminder)
            currentReminderY += 70
        }
        scrollView.contentSize = CGSizeMake(screen_width, currentReminderY)
        animateSort()
        print("END:Load")
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        start()
    }
    
    func start(){
        func createScrollView(){
            scrollView = UIScrollView(frame: CGRectMake(0,0,screen_width,screen_height))
            scrollView.backgroundColor = .rgba(44, 62, 80,1.0)
            scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped)))
            view.addSubview(scrollView)
        }
        func createTopBarView(){
            topBarView = UIView(frame: CGRectMake(0,0,screen_width,60))
            topBarView.backgroundColor = .rgba(142, 68, 173,1.0)
            view.addSubview(topBarView)
        }
        func createButtons(){
            filterImageView = UIImageView(frame: CGRectMake(10,screen_statusBar_height,35, 35))
            filterImageView.image = UIImage(named: "time_off.png")
            topBarView.addSubview(filterImageView)
            filterButton = UIButton(frame: CGRectMake(0,0,60,60))
            filterButton.addTarget(self, action: #selector(filterButtonWasPressed), forControlEvents: .TouchUpInside)
            view.addSubview(filterButton)
            
            let addImageView = UIImageView(frame: CGRectMake(screen_width-45,screen_statusBar_height,35, 35))
            addImageView.image = UIImage(named: "Add.png")
            topBarView.addSubview(addImageView)
            addButton = UIButton(frame: CGRectMake(screen_width-60,0,60,60))
            addButton.addTarget(self, action: #selector(addButtonWasPressed), forControlEvents: .TouchUpInside)
            view.addSubview(addButton)
            
            cancelImageView = UIImageView(frame: CGRectMake(10,screen_statusBar_height,35, 35))
            cancelImageView.image = UIImage(named: "Cancel.png")
            cancelImageView.hidden = true
            topBarView.addSubview(cancelImageView)
            addButton_cancel = UIButton(frame: CGRectMake(0,screen_statusBar_height,60,60))
            addButton_cancel.addTarget(self, action: #selector(cancelButtonWasPressed), forControlEvents: .TouchUpInside)
            addButton_cancel.enabled = false
            view.addSubview(addButton_cancel)
        }
        func createAddView(){
            addView = AddView()
            view.addSubview(addView)
            
            
        }
        func createDateColorPickerView(){
            datePicker = UIDatePicker(frame: CGRectMake(0, screen_height, screen_width,400))
            datePicker.addTarget(self, action: #selector(ViewController.datePickerValueChanged), forControlEvents: .ValueChanged)
            datePicker.minimumDate = NSDate(timeIntervalSinceNow: 0)
            datePicker.backgroundColor = UIColor.whiteColor()
            colorPicker = UIView(frame : CGRectMake(0, screen_height+400, screen_width, 60))
            colorPicker.backgroundColor = UIColor.blueColor()
            
            var colorTag = 1
            for color in ReminderColor().colors{
                
                
                colorTag+=1
            }
            
            view.addSubview(datePicker)
            view.addSubview(colorPicker)
            
        }
        
        createScrollView()
        loadReminders()
        createDateColorPickerView()
        createAddView()
        createTopBarView()
        createButtons()
       
    }
    
    func scrollViewTapped(){
        for reme in reminders {
            reme.textView.resignFirstResponder()
        }
        UIView.animateWithDuration(0.5) {
            datePicker.frame = CGRectMake(0, screen_height, screen_width, 400)
        }
        saveReminders()
    }
    
    func filterButtonWasPressed(){
        sortType = sortType == .time ? .importance : .time
        filterImageView.image = UIImage(named: sortType == .time ? "time_off.png" : "import_off.png")
        sort()
        animateSort()
    }
    
    func cancelButtonWasPressed(){
        filterButton.enabled = !filterButton.enabled
        filterImageView.hidden = !filterButton.enabled
        addButton_cancel.enabled = filterImageView.hidden
        cancelImageView.hidden = filterButton.enabled
        if cancelImageView.hidden {
            addView.reminder.textView.endEditing(true)
            UIView.animateWithDuration(0.5, animations: {
                self.addView.frame = CGRectMake(0, 60-screen_height, screen_width, screen_height-60)
            })
        }else{
            addView.opening()
        }
    }
    
    func addReminder(reminder : Reminder){
        reminders.append(reminder)
        reminder.tag = reminderTag
        reminderTag += 1
        sort()
        scrollView.addSubview(reminder)
        animateSort()
        scrollView.contentSize = CGSizeMake(screen_width, currentReminderY + 70)
        reminder.formatDate()
        currentReminderY += 70

    }
    
    func addButtonWasPressed(){
        if !cancelImageView.hidden{
            addReminder(Reminder(y: currentReminderY, data: addView.getReminderData()))
        }
        cancelButtonWasPressed()
        saveReminders()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sort(){
        func timeSort() -> [Reminder]{
            var sorted : [Reminder] = reminders
            do {var s = "\nTime PREsort: "; for x in sorted { s += "\(x.data.date.timeIntervalSinceNow), "}; print(s)}
            sorted.sortInPlace {$0.data.date.timeIntervalSinceNow < $1.data.date.timeIntervalSinceNow}
//            sorted.sortInPlace({$0.data.date.timeIntervalSinceNow > })
            do {var s = "Time POSTsort: "; for x in sorted { s += "\(x.data.date.timeIntervalSinceNow) "}; print(s)}
            return sorted
        }
        func colorSort() -> [Reminder]{
            var sorted : [Reminder] = reminders
            do {var s = "\nColor POSTsort: "; for x in sorted { s += "\(x.data.level) "}; print(s)}
            sorted.sortInPlace({ $0.data.level > $1.data.level })
            do {var s = "Color POSTsort: "; for x in sorted { s += "\(x.data.level) "}; print(s)}
            return sorted
        }
        switch sortType {
        case .importance:
            reminders = colorSort()
        case .time:
            reminders = timeSort()
        }
        
        
        
    }
    
    func animateSort(){
        for r in reminders {
            UIView.animateWithDuration(0.3, animations: {
                r.frame = CGRectMake(r.frame.origin.x, (CGFloat(self.reminders.indexOf(r)!) * 70) + 70, r.frame.size.width, r.frame.height)
            })
        }
    }
    
    func datePickerValueChanged(sender : AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd\nhh:mma"
        let strDate = dateFormatter.stringFromDate(datePicker.date)
        
        selectedReminder!.timeButton.setTitle(strDate, forState: .Normal)
        selectedReminder!.data.date = datePicker.date
        sort()
        animateSort()
    }
    


}

